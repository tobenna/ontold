function slide(e, opts) {
  var $next = $('li.next'),
    $prev = $('li.prev'),
    overlap = opts.overlap || 50,
    dir = opts.dir || 'right',
    $oldCurrent = $('li.current'),
    $oldPrev = $('li.prev'),
    $oldNext = $('li.next');

  e.stopPropagation();
  e.preventDefault();

  if(dir == 'right') {
    
    $oldPrev.css({
      'left': -($oldPrev.width() + 100)
    });
    
    $oldPrev.removeClass('active prev');
    $oldCurrent.removeClass('current').addClass('prev');
    $oldNext.removeClass('next').addClass('current');
    
    var $newCurrent = $('.current'),
        $newNext = $newCurrent.next();
    //If we are sliding to the last slide, set the next slide to the first slide
    $('.nav-next').prop('disabled') == $newNext.length > 0;
  
    
    $newNext.addClass('active next');
    
  } else {
    
    $oldNext.removeClass('active next');
    $oldCurrent.removeClass('current').addClass('next');
    $oldPrev.removeClass('prev').addClass('current');
    
     var $newCurrent = $('.current'),
         $newPrev = $newCurrent.prev();
    
    if($newPrev.length < 1) {
      console.log("we're on the first slide! Make the last slide the previous slide");
      $newNext = $('ul li:last-of-type');
    }
    
    $newPrev.addClass('active prev');
  }
  
   setSlides(opts.overlap);
  
}

function setSlides(overlap) {
  
  navToggle();
  
  $('li.current').css({
    'left': '50%'
  });
  
  $('li.next').css({
    'left': $(window).width() - overlap
  });
  
  $('.ahead').css({
    'left': $(window).width + 100
  });
  
  var $prev = $('li.prev');
  $prev.css({
    'left': -($prev.outerWidth() - overlap)
  });
  
  $('.behind').css({
    'left': -$(this).outerWidth() + 100
  });
  
}

function navToggle() {
  $('.nav-next').prop('disabled', !$('li.current').next().length);
  $('.nav-prev').prop('disabled', !$('li.current').prev().length);
}

$(function() {
  setSlides(50);

  $('.nav-next').click(function(e) {
    slide(e, {
      dir: 'right',
      overlap: 50
    });
  });

  $('.nav-prev').click(function(e) {
    slide(e, {
      dir: 'left',
      overlap: 50
    });
  });
  
});