describe User, type: :model do
  let!(:admin) { FactoryGirl.create(:admin) }
  it 'is expected to be an admin' do
    expect(admin).to be_admin
  end
end
