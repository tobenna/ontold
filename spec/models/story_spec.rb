describe Story, type: :model do
  it 'is not a valid story if name is shorter than 3 characters' do
    story = Story.new(title: 'xx')
    expect(story).to have(1).error_on(:title)
    expect(story).not_to be_valid
  end

  it 'validates url field' do
    story = Story.new(title: 'valid_title', url: 'invalid.com')
    expect(story).to have(1).error_on(:url)
    expect(story).not_to be_valid
  end

  it 'must be unique to be valid' do
    FactoryGirl.create(:collective)
    FactoryGirl.create(:user)
    story = FactoryGirl.create(:story)
    duplicate_story = Story.new(title: story.title)
    expect(duplicate_story).to have(1).error_on(:title)
  end
end
