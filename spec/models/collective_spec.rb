describe Collective, type: :model do
  it 'is not a valid collective if name is shorter than 3 characters' do
    collective = Collective.new(name: 'xx')
    expect(collective).to have(1).error_on(:name)
    expect(collective).not_to be_valid
  end

  it 'must be unique to be valid' do
    collective = FactoryGirl.create(:collective)
    duplicate_collective = Collective.new(name: collective.name)
    expect(duplicate_collective).to have(1).error_on(:name)
  end
end
