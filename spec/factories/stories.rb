include ActionDispatch::TestProcess
FactoryGirl.define do
    factory :story do
        title { Faker::StarWars.quote }
        url { Faker::Internet.url }
        description { Faker::Lorem.sentence }
        collective_id { Collective.first.id }
        user_id { User.first.id }
    end
end
