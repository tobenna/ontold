include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :collective do
    name { Faker::Book.title }
    date { Faker::Date.between(2.days.ago, Date.today) }
    issue_number { Faker::Number.between(1, 50)}
  end
end
