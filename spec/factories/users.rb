include ActionDispatch::TestProcess

FactoryGirl.define do
  site_password = Faker::Internet.password

  factory :admin, class: User do
    email { Faker::Internet.email }
    password { site_password }
    password_confirmation { site_password }
    full_name { Faker::Name.name }
    occupation { Faker::Name.title }
    website { Faker::Internet.url }
    admin { true }
  end

  factory :user do
    email { Faker::Internet.email }
    password { site_password }
    password_confirmation { site_password }
    full_name { Faker::Name.name }
    occupation { Faker::Name.title }
    website { Faker::Internet.url }
    admin { false }
  end
end
