feature 'stories' do
  let!(:collective) { FactoryGirl.create(:collective) }
  let!(:story_info) do {
      title: Faker::StarWars.quote,
      url: Faker::Internet.url,
      desc: Faker::Lorem.sentence
  }
  end

  let!(:admin) { FactoryGirl.create(:admin) }

  before :each do
    visit('/admin/users/sign_in')

    fill_in 'Email', with: admin.email
    fill_in 'Password', with: admin.password
    click_button 'Log in'
  end

  context 'creating a story' do
    scenario 'allows users to add a stories to the collective' do
      visit "/admin/collectives/#{collective.id}"
      click_link 'Add Story'
      fill_in 'Title', with: story_info[:title]
      fill_in 'Url',  with: story_info[:url]
      fill_in 'Description', with: story_info[:desc]
      click_button 'Create Story'
      expect(current_path).to eq "/admin/collectives/#{collective.id}"
      expect(page).to have_content(story_info[:title])
    end

    it 'does not allow for titles that are too short' do
      visit "/admin/collectives/#{collective.id}"
      click_link 'Add Story'
      fill_in 'Title', with: 'xy'
      fill_in 'Url',  with: story_info[:url]
      fill_in 'Description', with: story_info[:desc]
      click_button 'Create Story'
      expect(page).not_to have_content 'xy'
      expect(page).to have_content 'error'
    end

    it 'does not allow for invalid urls' do
      visit "/admin/collectives/#{collective.id}"
      click_link 'Add Story'
      fill_in 'Title', with: story_info[:title]
      fill_in 'Url',  with: 'not_a_url'
      fill_in 'Description', with: story_info[:desc]
      click_button 'Create Story'
      expect(page).not_to have_content 'xy'
      expect(page).to have_content 'error'
    end
  end
end

feature 'managing stories' do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:collective) { FactoryGirl.create(:collective) }
  let!(:story) { FactoryGirl.create(:story) }
  let!(:second_story) { FactoryGirl.create(:story) }
  before :each do
    visit('/admin/users/sign_in')

    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'
  end

  it 'user can delete own story' do
    visit "/admin/collectives/#{collective.id}/stories/#{story.id}"
    click_link 'Delete'
    expect(page).to have_content 'Story successfully deleted'
  end

  it 'user can edit own story' do
    visit "/admin/collectives/#{collective.id}/stories/#{story.id}"
    click_link 'Edit'
    fill_in 'Url', with: 'http://google.com/ng'
    click_button 'Update Story'
    expect(page).to have_content 'http://google.com/ng'
    expect(page).to have_content 'Story successfully updated'
  end
end
