feature 'User can sign in and out' do
  context 'user not signed in and on the homepage' do
    it "should see a 'sign in' link" do
      visit('/admin/users/sign_in')
      expect(page).to have_button('Log in')
    end

    it "should not see 'sign out' link" do
      visit('/admin/users/sign_in')
      expect(page).not_to have_link('Sign out')
    end
  end

  context 'user signed in on the homepage' do
    let!(:user) { FactoryGirl.create(:admin) }

    it "should see 'sign out' link" do
      visit('/admin/users/sign_in')
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'
      expect(page).to have_link('Sign out')
      expect(page).not_to have_link('Sign in')
    end
  end

  context 'admin creates a user' do
    let!(:admin) { FactoryGirl.create(:admin) }
    before do
      visit('/admin/users/sign_in')
      fill_in 'Email', with: admin.email
      fill_in 'Password', with: admin.password
      click_button 'Log in'
    end
    context 'admin fills the create user form and submits' do
      let!(:user) do
        double :user,
        email: 'user@user.com',
        password: 'password',
        name: Faker::Name.name
      end
      before do
        visit '/admin/users/registration'
        fill_in 'Email', with: user.email
        fill_in 'Full name', with: user.name
        fill_in 'Password', with: user.password
        fill_in 'Password confirmation', with: user.password
        click_button 'Create User'
      end
      it 'creates and displays a user/currator' do
        expect(current_path).to eq '/admin/users'
        expect(page).to have_content user.name
      end

      it 'prevents creation of duplicate users' do
        visit '/admin/users/registration'
        fill_in 'Email', with: user.email
        fill_in 'Password', with: user.password
        fill_in 'Password confirmation', with: user.password
        click_button 'Create User'
        expect(page).to have_content 'error'
      end

      context 'managing users' do
        let!(:first_user) { FactoryGirl.create(:user) }
        let!(:second_user) { FactoryGirl.create(:user) }
        scenario 'admin deletes a user' do
          within('div.user-item:first-of-type') do
            click_link 'Delete'
          end
          expect(page).not_to have_content user.name
        end
      end
    end

    context 'changing user field extras' do
      let!(:first_user) { FactoryGirl.create(:user) }
      scenario 'fills his form to change his details' do
        visit '/admin/users/extras/edit'
        fill_in 'Full name', with: 'Tobenna Ndu'
        click_button 'Update account'
        expect(page).not_to have_content first_user.full_name
        expect(page).to have_content 'Tobenna Ndu'
        expect(page).to have_content 'Successfully updated account'
      end
    end
  end
end

feature 'user management permissions' do
  let!(:admin) { FactoryGirl.create(:admin) }
  let!(:user) { FactoryGirl.create(:user) }
  let!(:second_user) { FactoryGirl.create(:user) }
  let!(:third_user) { FactoryGirl.create(:user) }
  let!(:collective) { FactoryGirl.create(:collective) }
  context 'signed in user is not admin' do
    before :each do
      visit('/admin/users/sign_in')

      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'
    end
    it 'cannot delete collective' do
      visit "/admin/collectives/#{collective.id}"
      expect(page).not_to have_link 'Delete'
    end
    it 'cannot delete users' do
      visit '/admin/users/'
      expect(page).not_to have_link 'Delete'
    end
  end
end
