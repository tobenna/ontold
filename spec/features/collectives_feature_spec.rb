feature 'collectives', :type => :request do
  context 'no collectives have been created' do
    scenario 'there should be no collectives' do
      get '/api/v1/collectives'
      body = JSON.parse(response.body)
      expect(body['collectives']).to be_empty
    end
  end

  context 'a collective has been added' do
    let!(:collective) { FactoryGirl.create(:collective) }

    scenario 'display the collectives' do
      get '/api/v1/collectives'
      collectives = JSON.parse(response.body)['collectives']
      first_collective = collectives.first['collective']
      expect(first_collective['name']).to eq collective.name
    end
  end
end

feature 'collectives manager' do
  let!(:admin) { FactoryGirl.create(:admin) }
  let!(:issue1) { FactoryGirl.create(:collective) }

  before :each do
    visit('/admin/users/sign_in')

    fill_in 'Email', with: admin.email
    fill_in 'Password', with: admin.password
    click_button 'Log in'
  end

  context 'creating a collective' do
    let(:collective) { double :collective, name: 'Trouble in little china' }
    scenario 'prompts a user to fill out a form; displays the collective' do
      visit '/admin/collectives/new'
      date = Faker::Date.between(2.days.ago, Date.today)
      fill_in 'Name', with: collective.name
      fill_in 'Date', with: date
      click_button 'Create Collective'
      expect(page).to have_content collective.name
    end
  end

  context 'viewing a collective' do
    before :each do
      visit '/admin/collectives'
    end
    scenario 'lets the admin view the collective' do
      click_link issue1.name
      expect(page).to have_content issue1.name
      expect(current_path).to eq "/admin/collectives/#{issue1.id}"
    end
  end

  context 'editing collectives' do
    scenario 'shows an edit form; updates collective' do
      visit '/admin/collectives'
      date = Faker::Date.between(2.days.ago, Date.today)
      click_link issue1.name
      click_link 'Edit'
      fill_in 'Name', with: 'New Name'
      fill_in 'Date', with: date
      click_button 'Update Collective'
      expect(page).to have_content 'New Name'
      expect(page).to have_content date
      expect(current_path).to eq "/admin/collectives/#{issue1.id}"
    end
  end

  context 'deleting collectives' do
    scenario 'removes a collective when the user clicks delete link' do
      visit "/admin/collectives/#{issue1.id}"
      click_link 'Delete'
      expect(page).not_to have_content issue1.name
      expect(current_path).to eq '/admin/collectives'
    end
  end
end
