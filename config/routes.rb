Rails.application.routes.draw do
  scope '/admin' do
    root to: 'admin/collectives#index'
    devise_for :users, skip: [:registrations]
    as :user do
      get 'users/edit', to: 'devise/registrations#edit', as: 'edit_user_registration'
      put 'users', to: 'devise/registrations#update', as: 'user_registration'
      get 'users/extras/edit', to: 'admin/registrations#edit', as: 'admin_edit_user_extras'
    end
    get 'users/registration', to: 'admin/registrations#new', as: 'new_user_registration'
    post 'users/', to: 'admin/registrations#create'
    get 'users/', to: 'admin/registrations#index', as: 'admin_users'
    patch 'users/:id', to: 'admin/registrations#update', as: 'admin_user'
    get 'users/mine', to: 'admin/registrations#show', as: 'admin_user_mine'
    delete 'users/registrations/:id', to: 'admin/registrations#destroy', as: 'destroy_user'
  end
  namespace :admin do
    resources :social_links
    resources :collectives do
      resources :stories
    end
  end
  get '/about', to: 'collectives#about_page', as: 'about_page'
  get '/contact', to: 'collectives#contact_page', as: 'contact_page'
  namespace :api do
    namespace :v1 do
      resources :collectives
      # get 'users/', to: 'registrations#index', as: 'api_users'
      get 'users/:id', to: 'registrations#show', as: 'api_user'
    end
  end
  get 'users/:id', to: 'registrations#show', as: 'show_user'
  get '/', to: 'collectives#home', as: 'index_home'
  resources :collectives
  get 'stories/:id', to: 'stories#show', as: 'story'
  get 'stories/tweet/:id', to: 'stories#tweet', as: 'story_tweet'
  get 'stories/share/:id', to: 'stories#share', as: 'story_share'
  get 'collectives/tweet/:id', to: 'collectives#tweet', as: 'collective_tweet'
  get 'collectives/share/:id', to: 'collectives#share', as: 'collective_share'
  get 'users/tweet/:id', to: 'registrations#tweet', as: 'user_tweet'
  get 'users/share/:id', to: 'registrations#share', as: 'user_share'

  get 'admin/collectives/:id/publish', to: 'admin/collectives#publish', as: 'collective_publish'
  get 'admin/collectives/:id/unpublish', to: 'admin/collectives#unpublish', as: 'collective_unpublish'
end
