class RemovePublishedFromCollective < ActiveRecord::Migration
  def change
    remove_column :collectives, :published, :boolean
  end
end
