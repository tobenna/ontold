class AddPublishedToCollectivesWithDefault < ActiveRecord::Migration
  def change
    add_column :collectives, :published, :boolean, default: false
  end
end
