class AddAttachmentImageToCollectives < ActiveRecord::Migration
  def self.up
    change_table :collectives do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :collectives, :image
  end
end
