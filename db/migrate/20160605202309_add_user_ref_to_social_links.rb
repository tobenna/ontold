class AddUserRefToSocialLinks < ActiveRecord::Migration
  def change
    add_reference :social_links, :user, index: true, foreign_key: true
  end
end
