class AddAccountTypeToSocialLinks < ActiveRecord::Migration
  def change
    add_column :social_links, :account_type, :string
  end
end
