class AddCollectiveRefToStories < ActiveRecord::Migration
  def change
    add_reference :stories, :collective, index: true, foreign_key: true
  end
end
