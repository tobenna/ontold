class AddFullNameWebsiteOccupationToUser < ActiveRecord::Migration
  def change
    add_column :users, :full_name, :string
    add_column :users, :website, :string
    add_column :users, :occupation, :string
  end
end
