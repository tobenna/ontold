class ChangeStoryDescNameToDescription < ActiveRecord::Migration
  def change
    rename_column :stories, :desc, :description
  end
end
