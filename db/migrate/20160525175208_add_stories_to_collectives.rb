class AddStoriesToCollectives < ActiveRecord::Migration
  def change
    add_reference :collectives, :collective, index: true, foreign_key: true
  end
end
