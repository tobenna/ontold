class AddIssueNumberToCollectives < ActiveRecord::Migration
  def change
    add_column :collectives, :issue_number, :string
  end
end
