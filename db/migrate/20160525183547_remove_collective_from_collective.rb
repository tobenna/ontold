class RemoveCollectiveFromCollective < ActiveRecord::Migration
  def change
    remove_column :collectives, :collective_id, :integer
  end
end
