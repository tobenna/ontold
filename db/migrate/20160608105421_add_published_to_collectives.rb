class AddPublishedToCollectives < ActiveRecord::Migration
  def change
    add_column :collectives, :published, :boolean, default: true
  end
end
