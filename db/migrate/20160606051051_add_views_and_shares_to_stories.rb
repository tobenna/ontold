class AddViewsAndSharesToStories < ActiveRecord::Migration
  def change
    add_column :stories, :views, :integer
    add_column :stories, :shares, :integer
  end
end
