class CreateSocialLinks < ActiveRecord::Migration
  def change
    create_table :social_links do |t|
      t.string :url

      t.timestamps null: false
    end
  end
end
