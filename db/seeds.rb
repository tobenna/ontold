# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
  User.create(full_name: 'Olukolapo', email: 'hi@kolapo.com', password: 'testtest', password_confirmation:'testtest', admin: true, occupation: 'UX/UI designer', website: 'http://kolapo.com')
