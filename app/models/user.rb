class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_attached_file :avatar, styles: { medium: '300x300>', thumb: '152x152>' }, default_url: '/images/:style/missing.png'
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :stories
  has_many :social_links

  def admin?
    admin
  end

  def views
    stories.map { |s| s.views }.reduce(:+)
  end

  def shares
    stories.map { |s| s.shares }.reduce(:+)
  end

  def finds
    stories.count
  end

  def website_to_s
    website.sub(/^https?\:\/\//, '').sub(/^www./,'')
  end

  def to_s
    full_name
  end
end
