class Story < ActiveRecord::Base
  belongs_to :user
  belongs_to :collective
  has_attached_file :image, styles: { medium: '300x300>', thumb: '100x100>' }, default_url: '/images/:style/missing.png'
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  validates :title, length: { minimum: 3, maximum: 50 }, uniqueness: true
  validates :url, format: URI.regexp(%w(http https))
  validates :description, length: { minimum: 3, maximum: 220 }, uniqueness: true
end
