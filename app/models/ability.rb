class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.admin?
      can :manage, :all
    elsif user.id
      can :update, SocialLink do |social_link|
        social_link.try(:user) == user
      end
      can :delete, SocialLink do |social_link|
        social_link.try(:user) == user
      end
      can :create, SocialLink
      can :read, :all
      can :create, Story
      can :update, Story do |story|
        story.try(:user) == user
      end
      can :destroy, Story do |story|
        story.try(:user) == user
      end
    else
      can :read, :admin
    end
  end
end
