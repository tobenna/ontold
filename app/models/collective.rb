class Collective < ActiveRecord::Base
  has_attached_file :image, styles: { medium: '300x300>', thumb: '100x100>' }, default_url: '/images/:style/missing.png'
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  has_many :stories
  validates :name, length: { minimum: 3, maximum: 26 }, uniqueness: true

  def date_ts
    date.to_time.strftime('%B %d, %Y')
  end

  def shares
    stories.map { |s| s.shares }.reduce(:+)
  end

  def views
    stories.map { |s| s.views }.reduce(:+)
  end
end
