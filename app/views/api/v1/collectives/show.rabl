collection @collective
attribute :name
node :date do |collective|
  collective.date_ts
end
attribute :issue_number
attribute :id
attribute :image
child :stories do
  attributes :title, :description, :id
  node :image do |story|
    story.image.url(:thumb)
  end
  child :user do
    attributes :full_name, :id
  end
end
