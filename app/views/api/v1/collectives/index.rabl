collection @collectives => :collectives
attribute :name
node :date do |collective|
  collective.date_ts
end
attribute :issue_number
attribute :image
