collection @user
attribute :full_name
attribute :occupation
attribute :id
attribute :website_to_s
attribute :website
child :stories do
  attributes :title, :description, :id
end
node :avatar_url do |u|
  u.avatar.url(:thumb)
end
