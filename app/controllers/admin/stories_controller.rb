class Admin::StoriesController < ApplicationController

  before_action :require_login

  def new
    @collective = Collective.find(params[:collective_id])
    @story = Story.new
    authorize! :create, Story
  end

  def index
  end

  def show
    @story = Story.find(params[:id])
    @collective = Collective.find(params[:collective_id])
    authorize! :read, Story
  end

  def create
    @collective = Collective.find(params[:collective_id])
    @story = @collective.stories.new(story_params)
    @story.user = current_user
    @story.shares = 0
    @story.views = 0
    authorize! :create, Story
    if @story.save
      redirect_to "#{collectives}/#{params[:collective_id]}"
    else
      render 'new'
    end
  end

  def edit
    @story = Story.find(params[:id])
    @collective = Collective.find(@story.collective_id)
    authorize! :update, @story
  end

  def update
    @story = Story.find(params[:id])
    @story.update(story_params)
    authorize! :update, @story
    flash[:notice] = 'Story successfully updated'
    redirect_to "/admin/collectives/#{params[:collective_id]}/stories/#{params[:id]}"
  end

  def destroy
    @story = Story.find(params[:id])
    authorize! :destroy, @story
    @story.destroy
    flash[:notice] = 'Story successfully deleted'
    redirect_to "#{collectives}/#{params[:collective_id]}"
  end

  private

  def story_params
    params.require(:story).permit(:title, :url, :description, :image)
  end

  def collectives
    '/admin/collectives'
  end

  def require_login
    unless current_user.id
      redirect_to new_user_session_path
    end
  end
end
