class Admin::CollectivesController < ApplicationController
  before_action :require_login
  load_and_authorize_resource
  def index
    @collectives = Collective.all
  end

  def new
    @collective = Collective.new
  end

  def show
  end

  def create
    Collective.create(collective_params)
    redirect_to '/admin/collectives'
  end

  def edit
  end

  def publish
    @collective.published = true
    @collective.save
    flash[:notice] = "Collective successfully published"
    redirect_to "/admin/collectives/#{params[:id]}"
  end

  def unpublish
    @collective.published = false
    @collective.save
    flash[:notice] = "Collective successfully unpublished"
    redirect_to "/admin/collectives/#{params[:id]}"
  end

  def update
    @collective.update(collective_params)
    flash[:notice] = 'Collective successfully updated'
    redirect_to "/admin/collectives/#{params[:id]}"
  end

  def destroy
    @collective.destroy
    flash[:notice] = 'Collective successfully deleted'
    redirect_to '/admin/collectives'
  end

  private
  def collective_params
    params.require(:collective).permit(:date, :name, :image, :issue_number)
  end

  def require_login
    unless current_user
      redirect_to new_user_session_path
    end
  end
end
