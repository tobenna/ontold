class Admin::RegistrationsController < ApplicationController

  before_action :require_login

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to '/admin/users'
    else
      render 'new'
    end
  end

  def index
    @users = User.all - [current_user]
  end

  def edit
    @registration = current_user
  end

  def show
    @user = current_user
  end

  def update
    current_user.update(user_params)
    flash[:notice] = 'Successfully updated account'
    redirect_to "/admin/users/mine"
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    flash[:notice] = 'User successfully deleted'
    redirect_to '/admin/users'
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :confirmation, :full_name, :occupation, :website, :avatar)
  end

  def require_login
    unless current_user
      redirect_to new_user_session_path
    end
  end
end
