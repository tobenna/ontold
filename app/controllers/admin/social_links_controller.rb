class Admin::SocialLinksController < ApplicationController

  def new
    @social_link = SocialLink.new
  end

  def create
    @social_link = current_user.social_links.new(social_link_params)
    if @social_link.save
      redirect_to admin_user_mine_path
    else
      render :new
    end
  end

  def edit
    @social_link = SocialLink.find(params[:id])
    authorize! :update, @social_link
  end

  def update
    @social_link = SocialLink.find(params[:id])
    authorize! :update, @social_link
    @social_link.update(social_link_params)
    flash[:notice] = 'Social Link successfully updated'
    redirect_to admin_user_mine_path
  end

  def destroy
    @social_link = SocialLink.find(params[:id])
    authorize! :destroy, @social_link
    @social_link.destroy
    flash[:notice] = 'Social Link successfully deleted'
    redirect_to admin_user_mine_path
  end

  private

  def social_link_params
    params.require(:social_link).permit(:url, :account_type)
  end

end
