class StoriesController < ApplicationController

  def show
    story = Story.find(params[:id])
    redirect_to '/' unless story.id
    story.views += 1
    story.save
    redirect_to story.url
  end

  def tweet
    story = Story.find(params[:id])
    redirect_to '/' unless story.id
    story.shares += 1
    story.save
    text = "#{story.title} #{story_url(story)}"
    redirect_to "https://twitter.com/intent/tweet?text=#{text}"
  end

  def share
    story = Story.find(params[:id])
    redirect_to '/' unless story.id
    story.shares += 1
    story.save
    referer = "#{story.title} #{story_url(story)}"
    redirect_to "http://www.facebook.com/sharer/sharer.php?u=#{referer}"
  end

end
