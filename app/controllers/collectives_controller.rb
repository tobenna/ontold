class CollectivesController < ApplicationController
  layout "front"
  def index
    @collectives = Collective.order(issue_number: :desc).where(published: true)
  end

  def show
    @collective = Collective.where(id: params[:id], published: true).first
    raise ActionController::RoutingError.new('Not Found') unless @collective 
  end

  def home
    @collectives = Collective.order(issue_number: :desc).where(published: true).first(8)
  end

  def about_page
    render :about
  end

  def contact_page
    render :contact
  end

  def tweet
    collective = Collective.find(params[:id])
    text = "#{collective.name} #{collective_url(collective)}"
    redirect_to "https://twitter.com/intent/tweet?text=#{text}"
  end

  def share
    collective = Collective.find(params[:id])
    referer = "#{collective_url(collective)}"
    redirect_to "http://www.facebook.com/sharer/sharer.php?u=#{referer}"
  end
end
