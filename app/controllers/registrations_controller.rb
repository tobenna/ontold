class RegistrationsController < ApplicationController
  layout "front"
  def show
    @user = User.find(params[:id])
  end

  def tweet
    user = User.find(params[:id])
    text = "#{user.full_name} #{show_user_url(user)}"
    redirect_to "https://twitter.com/intent/tweet?text=#{text}"
  end

  def share
    collective = User.find(params[:id])
    referer = "#{show_user_url(collective)}"
    redirect_to "http://www.facebook.com/sharer/sharer.php?u=#{referer}"
  end
end
