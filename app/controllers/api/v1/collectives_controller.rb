module Api
  module V1
    class CollectivesController < ApplicationController
      def index
        @collectives = Collective.all.reverse
      end

      def show
        @collective = Collective.find(params[:id])
      end
    end
  end
end
